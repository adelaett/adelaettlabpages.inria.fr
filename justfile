export CI_REGISTRY_IMAGE := "registry.gitlab.inria.fr/adelaett/adelaett.gitlabpages.inria.fr/zola"

update-image:
  nix build --impure
  docker image load < result
  docker push ${CI_REGISTRY_IMAGE}


build-image:
  nix build --impure