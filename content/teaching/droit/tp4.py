
# TP 4 = identifier les mauvais tris
####################################
#
# - des fonctions de tri sont données (dans le fichier tris.py)
#
# - chaque fonction a un paramètre (un tableau Python)
#   et est censée en modifier le contenu pour le trier par ordre croissant
#
# - une seule fonction est correcte et il s'agit de l'identifier
#

# on importe toutes les fonctions contenues dans tris.py
from tris import *
# elles s'appellent tri1, tri2, tri3, ..., tri10

# ensuite, on procède à du test *en boîte noire* i.e. sans regarder dans tris.py

# par exemple, on se donne un tableau
a = [3, 2, 1]
# on le passe à l'une des fonctions de tri
tri10(a)
# on affiche a ensuite pour vérifier s'il est bien trié
print(a)
