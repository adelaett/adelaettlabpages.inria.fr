+++
title = "Approche de l’élaboration et du fonctionnement des logiciels"
description = ""
date = 2025-02-20
draft = false
+++


Cours donné aux M2 "Droit de la création et du numérique" et "Droit de l'innovation de de la propriété intellectuelle"

[Glossaire](./glossaire.pdf)

Lien de l'environement JupyterLab: [ici](https://jupyterhub.ijclab.in2p3.fr/)

## **Cours 1**: Architecture des ordinateurs/Languages machines/Compilation

[slides](./cours1.pdf)
[activité](https://www.nandgame.com/)


## **Cours 2**: Programmation: variables/if-then-else/boucles

[slides](./cours2.pdf)
[aide-mémoire python](./python.pdf)




## **Cours 3**: Programmation: tableaux/fonctions

[slides](./cours3.pdf)


## **Cours 4**: La complexité du logiciel: bugs/tests/typage/complexité

[slides](./cours4.pdf)
[tp4.py](./tp4.py)
[tris.py](./tris.py)


## **Cours 5**: Intelligence artificelle/Internet

[base.html](./page.txt)
[aide-mémoire html/css](./html-css.pdf)

Exemples de site réalisés: [babi-green](./babi-green.html) [recette](./recette.html) [livres](./livres.html)


## **Cours 6**: Javascript/Bases de donnée
[aide-mémoire sql](./sql.pdf)
[Sujet de tp](./tp-sql.pdf)
[Correction du tp](./plus-tard)


## **Examen**: 21 mars 14h-16h salle 111

[sujet 2024](./exam-2024.pdf)
