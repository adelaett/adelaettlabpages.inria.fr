
### TP sur le test

# ce fichier contient des programmes de tri, appelés tri1, tri2, etc.

# chaque fonction prend en paramètre une liste d'entiers et la modifie
# pour en trier le contenu par ordre croissant

# par exemple,
#   a = [3, 2, 1]
#   tri(a)
#   print(a)
# va afficher
#   [1, 2, 3]

# objectif : identifier les fonctions qui ne sont pas correctes

from random import randint

def tri1(a):
    pass

def tri2(a):
    a.sort()
    if 0 in a:
        a.remove(0)

def tri3(a):
    a.sort()
    for i in range(len(a)):
        a[i] += 1

def tri4(a):
    a.sort()
    for i in range(len(a)):
        if a[i] < 0:
            a[i] = -a[i]

def tri5(a):
    a.sort()
    if len(a) > 1:
        a[0], a[1] = a[1], a[0]

def tri6(a):
    a.sort()
    if len(a) > 3:
        a.pop()

def tri7(a):
    a.sort()

def tri8(a):
    a.sort(reverse=True)

def tri9(a):
    a.sort()
    for i in range(len(a) - 1):
        if a[i] == a[i+1]:
            a[i+1] += 1

def tri10(a):
    for i in range(len(a)):
        a[i] = i+1

def tri11(a):
    a.sort()
    for i in range(len(a)):
        a[i] *= 2

def tri12(a):
    b = sorted(a)
    for i in range(len(a)):
        a[i] = b[i] + 1

def tri13(a):
    a.sort()
    for i in range(len(a)):
        if a[i] % 2 == 0:
            a[i] //= 2

def tri14(a):
    a.sort()
    a.append(0)

def tri15(a):
    a.sort()
    for i in range(len(a)):
        if a[i] == max(a):
            a[i] = 0

def tri16(a):
    a.sort()
    for i in range(len(a)):
        a[i] = -a[i]

def tri17(a):
    for i in range(len(a)//2):
        a[i], a[-i-1] = a[-i-1], a[i]

def tri18(a):
    a.sort()
    for i in range(1, len(a), 2):
        a[i], a[i-1] = a[i-1], a[i]

def tri19(a):
    a.sort()
    for i in range(len(a)):
        if a[i] == min(a):
            a[i] += 1

def tri20(a):
    a.sort()
    a.insert(0, a.pop())


def tri21(a):
    a.sort()
    a.reverse()

def tri22(a):
    n = len(a)
    for i in range(n):
        for j in range(n-1):
            if a[j] > a[j+1]:
                a[j], a[j+1] = a[j+1], a[j]

def tri23(a):
    for i in range(1, len(a)):
        key = a[i]
        j = i - 1
        while j >= 0 and key < a[j]:
            a[j + 1] = a[j]
            j -= 1
        a[j + 1] = key

def tri24(a):
    for i in range(len(a)):
        min_idx = i
        for j in range(i+1, len(a)):
            if a[min_idx] > a[j]:
                min_idx = j
        a[i], a[min_idx] = a[min_idx], a[i]
    a.append(0)

def tri25(a):
    def partition(arr, low, high):
        i = (low-1)
        pivot = arr[high]
        for j in range(low, high):
            if arr[j] <= pivot:
                i = i+1
                arr[i], arr[j] = arr[j], arr[i]
        arr[i+1], arr[high] = arr[high], arr[i+1]
        return (i+1)
    
    def quickSort(arr, low, high):
        if len(arr) == 1:
            return arr
        if low < high:
            pi = partition(arr, low, high)
            quickSort(arr, low, pi-1)
            quickSort(arr, pi+1, high)

    quickSort(a, 0, len(a)-1)
    a.append(0)

def tri26(a):
    def merge(left, right):
        result = []
        i = j = 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                result.append(left[i])
                i += 1
            else:
                result.append(right[j])
                j += 1
        result.extend(left[i:])
        result.extend(right[j:])
        return result
    
    def mergeSort(arr):
        if len(arr) <= 1:
            return arr
        mid = len(arr) // 2
        left = mergeSort(arr[:mid])
        right = mergeSort(arr[mid:])
        return merge(left, right)

    a[:] = mergeSort(a)
    for i in range(len(a)):
        a[i] += 1

def tri27(a):
    def heapify(arr, n, i):
        largest = i
        l = 2 * i + 1
        r = 2 * i + 2
        if l < n and arr[i] < arr[l]:
            largest = l
        if r < n and arr[largest] < arr[r]:
            largest = r
        if largest != i:
            arr[i], arr[largest] = arr[largest], arr[i]
            heapify(arr, n, largest)

    def heapSort(arr):
        n = len(arr)
        for i in range(n // 2 - 1, -1, -1):
            heapify(arr, n, i)
        for i in range(n-1, 0, -1):
            arr[i], arr[0] = arr[0], arr[i]
            heapify(arr, i, 0)

    heapSort(a)
    a.pop()

def tri28(a):
    def inefficient_sort(arr):
        sorted_arr = []
        while arr:
            minimum = min(arr)
            sorted_arr.append(minimum)
            arr.remove(minimum)
        return sorted_arr

    a[:] = inefficient_sort(a)

def tri29(a):
    n = len(a)
    for i in range(n):
        for j in range(0, n-i-1):
            if a[j] > a[j+1]:
                a[j], a[j+1] = a[j+1], a[j]
    for i in range(n):
        for j in range(0, n-i-1):
            if a[j] > a[j+1]:
                a[j], a[j+1] = a[j+1], a[j]

def tri30(a):
    n = len(a)
    for i in range(n):
        min_idx = i
        for j in range(i+1, n):
            if a[min_idx] > a[j]:
                min_idx = j
        a[i], a[min_idx] = a[min_idx], a[i]
    for i in range(n):
        min_idx = i
        for j in range(i+1, n):
            if a[min_idx] > a[j]:
                min_idx = j
        a[i], a[min_idx] = a[min_idx], a[i]
