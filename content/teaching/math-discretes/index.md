+++
title = "Mathématiques Discrètes Groupe 11"
description = ""
date = 2024-09-10
draft = false
+++

Cours de L2, mathématiques discrètes. [Page officielle du cours](https://www-licence.ufr-info-p6.jussieu.fr/lmd/licence/2024/ue/LU2IN005-2024oct/)

Les annales corrigées ainsi que les exercices de TD (non corrigés) sont disponible sur la page officielle du cours. Ces informations sont également disponible sur le [moodle](https://moodle-sciences-24.sorbonne-universite.fr/course/view.php?id=2283) avec en plus des **fiches**. Ces fiches sont **indispensable** pour bien comprendre comment rédiger les exercices.

Je vous invite à _poser des questions_ par mail. À chaque fois qu'une personne m'a posé une question par mail et que j'avais un peu de temps, j'ai écrit un document pour répondre expliciement et exhaustivement à la question.

Voici quelques documents que j'ai écrit:

**Ensembles, relations, applications**
* [Produit cartésien](./relations.pdf)
* [Les differents types de relations](./relations.pdf)

**Relations d'ordres, induction(s)**
* [Applications monotones et ensembles isomorphes](./monotone.pdf)
* [Différences récurrence forte/récurrence faible](./induction.pdf)
* [Induction bien fondée](./bien-fondé.pdf)

**Automates**
* [Automates & lemme de l'étoile](./etoile.pdf)
* [Expression régulières](./regex.pdf)

Chacun de ces documents a été rédigé en utilisant [typst](https://github.com/typst/typst). Je trouve ça beaucoup plus somple que d'utiliser LaTeX: vous pouvez déjà le télécharger et l'installer pour écrire des documents.


## Ressources complémentaires

Voici quelques documents que j'ai partagé à certains d'entre vous. Ces conseils s'appliquent aux autres cours

- [Conseil d'aide à la rédaction](https://www.bibmath.net/ressources/index.php?action=affiche&quoi=conseils_redac)

- [Liste des cours au collège de france](https://www.college-de-france.fr/en/chair/thierry-coquand-computer-sciences-and-digital-technologies-annual-chair) Je serais présent à ces cours ainsi qu'a sa lecture inaugurale. C'est juste à coté de Jussieu. Allez-y.

- [Regex 101](https://regex101.com/) est un site internet sur lequel vous pouvez visualiser les regex sur un exemple de texte. Les regex des langages de programmations "normaux" sont differents des regex que l'on a vu en math discrètes, mais ils y ressemblent de loin.

- [DM non distribué](./dm-en-plus.pdf) est un dm que j'avais rédigé avant de connaitre les attentes du cours. Il est *très* difficile et touche à plusieurs notions vus en cours et que vous pouvez voir en probabilités. L'idée est de regarder des propriétées des automates fini probabilistes.


## Correction de l'examen

J'ai, après avoir corrigé mes copies, écrit une description de quelles questions avaient été réussies. Le texte est pas forcément très gentil: je me suis basé sur le style des rapports d'agreg: ["Cette question n'a pas eu un franc succès"](https://agreg.org/data/uploads/rapports/rapport_2021.pdf#subsection.3.2.2). Ne le prennez pas contre vous, voyez le comme une façon d'assainir ma frustration (même si les copies dont j'ai reconnu l'écriture étaient tout à fait excellentes). Ce n'est pas de vous que je parle, mais de certaines copies de vos camarades dans d'autres groupes.

Le [fichier](./commentaires-exam.pdf).
