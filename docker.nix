{ dockerTools, zola, busybox, name}:
dockerTools.buildLayeredImage {
  name = name;
  tag = "latest";
  contents = [ zola busybox ];
}
